var wms_layers = [];


        var lyr_OpenStreetMap_0 = new ol.layer.Tile({
            'title': 'OpenStreetMap',
            'type': 'base',
            'opacity': 1.000000,
            
            
            source: new ol.source.XYZ({
    attributions: ' ',
                url: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png'
            })
        });
var format_PuntosPickit_1 = new ol.format.GeoJSON();
var features_PuntosPickit_1 = format_PuntosPickit_1.readFeatures(json_PuntosPickit_1, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_PuntosPickit_1 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_PuntosPickit_1.addFeatures(features_PuntosPickit_1);
var lyr_PuntosPickit_1 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_PuntosPickit_1, 
                style: style_PuntosPickit_1,
                interactive: true,
                title: '<img src="styles/legend/PuntosPickit_1.png" /> PuntosPickit'
            });
var format_areaPPsuperpuesta_2 = new ol.format.GeoJSON();
var features_areaPPsuperpuesta_2 = format_areaPPsuperpuesta_2.readFeatures(json_areaPPsuperpuesta_2, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_areaPPsuperpuesta_2 = new ol.source.Vector({
    attributions: ' ',
});
jsonSource_areaPPsuperpuesta_2.addFeatures(features_areaPPsuperpuesta_2);
var lyr_areaPPsuperpuesta_2 = new ol.layer.Vector({
                declutter: true,
                source:jsonSource_areaPPsuperpuesta_2, 
                style: style_areaPPsuperpuesta_2,
                interactive: true,
    title: 'areaPPsuperpuesta<br />\
    <img src="styles/legend/areaPPsuperpuesta_2_0.png" /> <br />\
    <img src="styles/legend/areaPPsuperpuesta_2_1.png" /> <br />\
    <img src="styles/legend/areaPPsuperpuesta_2_2.png" /> <br />\
    <img src="styles/legend/areaPPsuperpuesta_2_3.png" /> <br />'
        });

lyr_OpenStreetMap_0.setVisible(true);lyr_PuntosPickit_1.setVisible(true);lyr_areaPPsuperpuesta_2.setVisible(true);
var layersList = [lyr_OpenStreetMap_0,lyr_PuntosPickit_1,lyr_areaPPsuperpuesta_2];
lyr_PuntosPickit_1.set('fieldAliases', {'id': 'id', 'nombre': 'nombre', 'direccion': 'direccion', 'direccion_custom': 'direccion_custom', 'codigo_postal': 'codigo_postal', 'telefono': 'telefono', 'nombre_provincia': 'nombre_provincia', 'localidad': 'localidad', 'fecha_creacion': 'fecha_creacion', 'latitud': 'latitud', 'longitud': 'longitud', 'estado': 'estado', 'id_pais': 'id_pais', 'fecha_creacion_archivo': 'fecha_creacion_archivo', 'id_1': 'id_1', 'id_base': 'id_base', 'fecha_activacion': 'fecha_activacion', 'estado_1': 'estado_1', 'cadena': 'cadena', 'visible_lightbox': 'visible_lightbox', 'id_pais_1': 'id_pais_1', 'fecha_creacion_archivo_1': 'fecha_creacion_archivo_1', });
lyr_areaPPsuperpuesta_2.set('fieldAliases', {'fid': 'fid', 'id': 'id', 'nombre': 'nombre', 'direccion': 'direccion', 'direccion_custom': 'direccion_custom', 'codigo_postal': 'codigo_postal', 'telefono': 'telefono', 'nombre_provincia': 'nombre_provincia', 'localidad': 'localidad', 'fecha_creacion': 'fecha_creacion', 'latitud': 'latitud', 'longitud': 'longitud', 'estado': 'estado', 'id_pais': 'id_pais', 'fecha_creacion_archivo': 'fecha_creacion_archivo', 'id_1': 'id_1', 'id_base': 'id_base', 'fecha_activacion': 'fecha_activacion', 'estado_1': 'estado_1', 'cadena': 'cadena', 'visible_lightbox': 'visible_lightbox', 'id_pais_1': 'id_pais_1', 'fecha_creacion_archivo_1': 'fecha_creacion_archivo_1', 'coso': 'coso', });
lyr_PuntosPickit_1.set('fieldImages', {'id': 'Range', 'nombre': 'TextEdit', 'direccion': 'TextEdit', 'direccion_custom': 'TextEdit', 'codigo_postal': 'Range', 'telefono': 'TextEdit', 'nombre_provincia': 'TextEdit', 'localidad': 'TextEdit', 'fecha_creacion': 'DateTime', 'latitud': 'TextEdit', 'longitud': 'TextEdit', 'estado': 'Range', 'id_pais': 'Range', 'fecha_creacion_archivo': 'DateTime', 'id_1': 'Range', 'id_base': 'Range', 'fecha_activacion': 'DateTime', 'estado_1': 'Range', 'cadena': 'TextEdit', 'visible_lightbox': 'TextEdit', 'id_pais_1': 'Range', 'fecha_creacion_archivo_1': 'DateTime', });
lyr_areaPPsuperpuesta_2.set('fieldImages', {'fid': 'Range', 'id': 'Range', 'nombre': 'TextEdit', 'direccion': 'TextEdit', 'direccion_custom': 'TextEdit', 'codigo_postal': 'Range', 'telefono': 'TextEdit', 'nombre_provincia': 'TextEdit', 'localidad': 'TextEdit', 'fecha_creacion': 'DateTime', 'latitud': 'TextEdit', 'longitud': 'TextEdit', 'estado': 'Range', 'id_pais': 'Range', 'fecha_creacion_archivo': 'DateTime', 'id_1': 'Range', 'id_base': 'Range', 'fecha_activacion': 'DateTime', 'estado_1': 'Range', 'cadena': 'TextEdit', 'visible_lightbox': 'TextEdit', 'id_pais_1': 'Range', 'fecha_creacion_archivo_1': 'DateTime', 'coso': 'Range', });
lyr_PuntosPickit_1.set('fieldLabels', {'id': 'no label', 'nombre': 'no label', 'direccion': 'no label', 'direccion_custom': 'no label', 'codigo_postal': 'no label', 'telefono': 'no label', 'nombre_provincia': 'no label', 'localidad': 'no label', 'fecha_creacion': 'no label', 'latitud': 'no label', 'longitud': 'no label', 'estado': 'no label', 'id_pais': 'no label', 'fecha_creacion_archivo': 'no label', 'id_1': 'no label', 'id_base': 'no label', 'fecha_activacion': 'no label', 'estado_1': 'no label', 'cadena': 'no label', 'visible_lightbox': 'no label', 'id_pais_1': 'no label', 'fecha_creacion_archivo_1': 'no label', });
lyr_areaPPsuperpuesta_2.set('fieldLabels', {'fid': 'no label', 'id': 'no label', 'nombre': 'no label', 'direccion': 'no label', 'direccion_custom': 'no label', 'codigo_postal': 'no label', 'telefono': 'no label', 'nombre_provincia': 'no label', 'localidad': 'no label', 'fecha_creacion': 'no label', 'latitud': 'no label', 'longitud': 'no label', 'estado': 'no label', 'id_pais': 'no label', 'fecha_creacion_archivo': 'no label', 'id_1': 'no label', 'id_base': 'no label', 'fecha_activacion': 'no label', 'estado_1': 'no label', 'cadena': 'no label', 'visible_lightbox': 'no label', 'id_pais_1': 'no label', 'fecha_creacion_archivo_1': 'no label', 'coso': 'no label', });
lyr_areaPPsuperpuesta_2.on('precompose', function(evt) {
    evt.context.globalCompositeOperation = 'normal';
});